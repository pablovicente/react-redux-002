var io = require('socket.io')(10000);


function sendTime() {
  console.log('Sending time');
  io.emit('now', { now: new Date().toJSON() });
}

setInterval(sendTime, 3000);

io.on('connection', function (socket) {
  console.log('Client connected');
});