/*
 * action types
 */

export const DEVICE_SELECTED = 'DEVICE_SELECTED';
export const DEVICE_UNSELECTED = 'DEVICE_UNSELECTED';

/*
 * other constants
 */

export const deviceActions = {
  DEVICE_SELECTED: 'DEVICE_SELECTED',
  DEVICE_UNSELECTED: 'DEVICE_UNSELECTED'
};

/*
 * action creators
 */

export function selectDevice(payload) {
  return { type: DEVICE_SELECTED, payload }
}

export function unSelectDevice(payload) {
  return { type: DEVICE_UNSELECTED, payload }
}