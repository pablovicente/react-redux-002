import {combineReducers} from 'redux';
import devicesReducer from './devices-reducer.js';
import selectedDevice from './selected-device-reducer';

const allReducers = combineReducers(
  {
    devices: devicesReducer,
    selectedDevice: selectedDevice
  }
);

export default allReducers;