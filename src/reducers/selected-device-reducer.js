export default function(state=null, action){
  switch(action.type){
    case 'DEVICE_SELECTED':
      return action.payload;
    case 'DEVICE_UNSELECTED':
      return action.payload;
    default:
      break;
  }
  return state; //default
}