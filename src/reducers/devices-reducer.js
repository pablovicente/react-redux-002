export default function(){
  return [
    {
      id: 1,
      status: 'online',
      lat: 32.3,
      lng: 54.3,
      selected: false
    },
    {
      id: 2,
      status: 'offline',
      lat: 32.9,
      lng: 53.3,
      selected: false
    },
    {
      id: 3,
      status: 'online',
      lat: 31.7,
      lng: 52.9,
      selected: false
    }
  ];
}