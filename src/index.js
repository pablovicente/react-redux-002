import React from 'react';
import ReactDOM from 'react-dom';
// Redux
import {Provider} from 'react-redux'
import {createStore} from 'redux';
import allReducers from './reducers/index';
// Redux
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';


const store = createStore(
  allReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
document.getElementById('root')
)
;
registerServiceWorker();
