import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { selectDevice, unSelectDevice } from '../actions/index'

import '../css/device.css';

class Device extends Component{
  createListItems(){
    //return this.props.devices.map((aDevice) => <div className='device' onMouseDown={ () => this.props.unSelectDevice({})} onClick={ () => this.props.selectDevice(aDevice)} key={aDevice.id}><li>{aDevice.status}</li></div>)
    return this.props.devices.map((aDevice) => <div className='device' onClick={ () => this.props.selectDevice(aDevice)} key={aDevice.id}><li>{aDevice.status}</li></div>)
  }

  render(){
    return (
      <div>
        {this.createListItems()}
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    devices: state.devices
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectDevice, unSelectDevice}, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Device);